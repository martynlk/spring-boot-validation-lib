package com.martynlk.springbootbeanvalidation.annotation;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.executable.ExecutableValidator;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@RunWith(SpringRunner.class)
@SpringBootTest
public class IsNullTests {

    @Autowired
    private ValidatorFactory validatorFactory;
    private Validator validator;
    private ExecutableValidator executableValidator;

    public class TestClass {

        @IsNull public Object testField;

        public void parameterTestMethod(@IsNull Object parameterToTest){}

        @IsNull
        public Object returnValueTestMethod(Object objectToReturn){
            return objectToReturn;
        }
    }

    @Before
    public void setup() {
        this.validator = this
            .validatorFactory
            .getValidator()
        ;

        this.executableValidator = validator.forExecutables();

        LocaleContextHolder.resetLocaleContext();
    }

    @Test
    public void fieldValidation_fieldIsNull_noViolationsOccur(){

        // Given
        TestClass testClass = new TestClass();
        testClass.testField = null;

        // When
        Set<ConstraintViolation<TestClass>> violations = validator.validate(testClass);

        // Assert
        assertEquals(0, violations.size());
    }

    @Test
    public void fieldValidation_fieldIsNotNull_oneViolationOccurs(){

        // Given
        TestClass testClass = new TestClass();
        testClass.testField = "not null";

        // When
        Set<ConstraintViolation<TestClass>> violations = validator.validate(testClass);

        // Expect
        String expectedMessage = "This value should be null, but it has a value of 'not null'.";

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<TestClass> violation = violations.iterator().next();
        assertEquals(IsNull.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals(expectedMessage, violation.getMessage());
    }

    @Test
    public void parameterValidation_parameterIsNull_noViolationsOccur(){

        try {
            // Given
            TestClass testClass = new TestClass();

            // When
            Set<ConstraintViolation<TestClass>> violations = executableValidator
                .validateParameters(
                    testClass,
                    TestClass.class.getMethod("parameterTestMethod", Object.class),
                    new Object[]{null}
                );

            // Assert
            assertEquals(0, violations.size());

        } catch (NoSuchMethodException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void parameterValidation_parameterIsNotNull_oneViolationOccurs(){

        try {
            // Given
            TestClass testClass = new TestClass();

            // When
            Set<ConstraintViolation<TestClass>> violations = executableValidator
                .validateParameters(
                    testClass,
                    TestClass.class.getMethod("parameterTestMethod", Object.class),
                    new Object[]{ "not null" }
                );

            // Expect
            String expectedMessage = "This value should be null, but it has a value of 'not null'.";

            // Assert
            assertEquals(1, violations.size());

            ConstraintViolation<TestClass> violation = violations.iterator().next();
            assertEquals(IsNull.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
            assertEquals(expectedMessage, violation.getMessage());

        } catch (NoSuchMethodException e) {
            fail(e.getMessage());
        }

    }

    @Test
    public void returnValueValidation_returnValueIsNull_noViolationsOccur(){

        try {
            // Given
            TestClass testClass = new TestClass();

            // When
            Set<ConstraintViolation<TestClass>> violations = executableValidator
                .validateReturnValue(
                    testClass,
                    TestClass.class.getMethod("returnValueTestMethod", Object.class),
                    null
                );

            // Assert
            assertEquals(0, violations.size());

        } catch (NoSuchMethodException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void returnValueValidation_returnValueIsNotNull_oneViolationOccurs(){

        try {
            // Given
            TestClass testClass = new TestClass();

            // When
            Set<ConstraintViolation<TestClass>> violations = executableValidator
                .validateReturnValue(
                    testClass,
                    TestClass.class.getMethod("returnValueTestMethod", Object.class),
                    "not null"
                );

            // Expect
            String expectedMessage = "This value should be null, but it has a value of 'not null'.";

            // Assert
            assertEquals(1, violations.size());

            ConstraintViolation<TestClass> violation = violations.iterator().next();
            assertEquals(IsNull.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
            assertEquals(expectedMessage, violation.getMessage());

        } catch (NoSuchMethodException e) {
            fail(e.getMessage());
        }

    }
}
