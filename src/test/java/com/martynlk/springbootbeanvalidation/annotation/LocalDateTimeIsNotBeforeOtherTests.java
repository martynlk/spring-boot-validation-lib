package com.martynlk.springbootbeanvalidation.annotation;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.time.LocalDateTime;
import java.util.Set;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LocalDateTimeIsNotBeforeOtherTests {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Autowired
    private ValidatorFactory validatorFactory;

    private Validator validator;
    
    @LocalDateTimeIsNotBeforeOther(propertyNameToCheck = "x", propertyNameToCheckAgainst = "y")
    public class TestClass {
        
        private LocalDateTime x;
        private LocalDateTime y;

        public LocalDateTime getX() {
            return x;
        }

        public void setX(LocalDateTime x) {
            this.x = x;
        }

        public LocalDateTime getY() {
            return y;
        }

        public void setY(LocalDateTime y) {
            this.y = y;
        }
    }

    @LocalDateTimeIsNotBeforeOther(propertyNameToCheck = "x", propertyNameToCheckAgainst = "a")
    public class TestClassWithNoAccessorForPropertyToCheck extends TestClass { }
    
    @Before
    public void setup() {
        this.validator = this
            .validatorFactory
            .getValidator()
        ;
        
        LocaleContextHolder.resetLocaleContext();
    }

    @Test
    public void validate_objectWithPropertiesToValidateIsNull_illegalArgumentExceptionThrown(){

        // Given
        TestClass testClass = null;

        // Expect
        exception.expect(IllegalArgumentException.class);

        // When
        validator.validate(testClass);
    }

    @Test
    public void validate_objectWithPropertiesIsNotNullButExceptionThrownWhenPropertyAccessed_runtimeExceptionThrown(){

        // Given
        TestClassWithNoAccessorForPropertyToCheck testClass = new TestClassWithNoAccessorForPropertyToCheck();

        // Expect
        exception.expect(RuntimeException.class);

        // When
        validator.validate(testClass);
    }

    @Test
    public void validate_bothPropertiesAreNull_noViolationsOccur(){

        // Given
        TestClass testClass = new TestClass();
        testClass.setX(null);
        testClass.setY(null);

        // When
        Set<ConstraintViolation<TestClass>> violations = validator.validate(testClass);

        // Assert
        assertEquals(0, violations.size());
    }

    @Test
    public void validate_xIsNullButYIsNot_noViolationsOccur(){

        // Given
        TestClass testClass = new TestClass();
        testClass.setX(null);
        testClass.setY(LocalDateTime.now());

        // When
        Set<ConstraintViolation<TestClass>> violations = validator.validate(testClass);

        // Assert
        assertEquals(0, violations.size());
    }

    @Test
    public void validate_xIsNotNullButYIs_noViolationsOccur(){

        // Given
        TestClass testClass = new TestClass();
        testClass.setX(LocalDateTime.now());
        testClass.setY(null);

        // When
        Set<ConstraintViolation<TestClass>> violations = validator.validate(testClass);

        // Assert
        assertEquals(0, violations.size());
    }
    
    @Test
    public void validate_xIsBeforeY_oneViolationOccurs() {
        
        // Given
        LocalDateTime x = LocalDateTime.of(2018, 3, 12, 22, 12, 37);
        LocalDateTime y = x.plusSeconds(1l);
        
        TestClass test = new TestClass();
        test.setX(x);
        test.setY(y);
        
        // When
        Set<ConstraintViolation<TestClass>> violations = validator.validate(test);
        
        // Expect
        String expectedMessage = "The date of property 'x' (" + x.toString() + ") is before the date of property 'y' " +
            "(" + y.toString() + ")."
        ;
        
        // Assert
        assertEquals(1, violations.size());
        
        ConstraintViolation<TestClass> violation = violations.iterator().next();
        assertEquals(LocalDateTimeIsNotBeforeOther.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals(expectedMessage, violation.getMessage());
    }
    
    @Test
    public void validate_xIsEqualToY_noViolationsOccur() {
        
        // Given
        LocalDateTime x = LocalDateTime.of(2018, 3, 12, 22, 12, 37);
        LocalDateTime y = x;

        TestClass test = new TestClass();
        test.setX(x);
        test.setY(y);
        
        // When
        Set<ConstraintViolation<TestClass>> violations = validator.validate(test);
        
        // Assert
        assertEquals(0, violations.size());
    }
    
    @Test
    public void validate_xIsAfterY_noViolationsOccur() {
        
        // Given
        LocalDateTime x = LocalDateTime.of(2018, 3, 12, 22, 12, 37);
        LocalDateTime y = x.minusSeconds(1l);

        TestClass test = new TestClass();
        test.setX(x);
        test.setY(y);
        
        // When
        Set<ConstraintViolation<TestClass>> violations = validator.validate(test);
        
        // Assert
        assertEquals(0, violations.size());
    }
}
