package com.martynlk.springbootbeanvalidation.annotation;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class NumberPropertyIsNotGreaterThanOtherTests {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Autowired
    private ValidatorFactory validatorFactory;

    private Validator validator;

    @NumberPropertyIsNotGreaterThanOther(propertyToCheck = "x", propertyToCheckAgainst = "y")
    public class TestClass{

        private Number x;
        private Number y;

        public Number getX() {
            return x;
        }

        public void setX(Number x) {
            this.x = x;
        }

        public Number getY() {
            return y;
        }

        public void setY(Number y) {
            this.y = y;
        }
    }

    @NumberPropertyIsNotGreaterThanOther(propertyToCheck = "x", propertyToCheckAgainst = "a")
    public class TestClassWithNoAccessorForPropertyToCheck extends TestClass { }

    @Before
    public void setup() {
        this.validator = this
            .validatorFactory
            .getValidator()
        ;

        LocaleContextHolder.resetLocaleContext();
    }

    @Test
    public void validate_objectWithPropertiesToValidateIsNull_illegalArgumentExceptionThrown(){

        // Given
        TestClass testClass = null;

        // Expect
        exception.expect(IllegalArgumentException.class);

        // When
        validator.validate(testClass);
    }

    @Test
    public void validate_objectWithPropertiesIsNotNullButExceptionThrownWhenPropertyAccessed_runtimeExceptionThrown(){

        // Given
        TestClassWithNoAccessorForPropertyToCheck testClass = new TestClassWithNoAccessorForPropertyToCheck();

        // Expect
        exception.expect(RuntimeException.class);

        // When
        validator.validate(testClass);
    }

    @Test
    public void validate_bothPropertiesAreNull_noViolationsOccur(){

        // Given
        TestClass testClass = new TestClass();
        testClass.setX(null);
        testClass.setY(null);

        // When
        Set<ConstraintViolation<TestClass>> violations = validator.validate(testClass);

        // Assert
        assertEquals(0, violations.size());
    }

    @Test
    public void validate_xIsNullButYIsNot_noViolationsOccur(){

        // Given
        TestClass testClass = new TestClass();
        testClass.setX(null);
        testClass.setY(1);

        // When
        Set<ConstraintViolation<TestClass>> violations = validator.validate(testClass);

        // Assert
        assertEquals(0, violations.size());
    }

    @Test
    public void validate_xIsNotNullButYIs_noViolationsOccur(){

        // Given
        TestClass testClass = new TestClass();
        testClass.setX(1);
        testClass.setY(null);

        // When
        Set<ConstraintViolation<TestClass>> violations = validator.validate(testClass);

        // Assert
        assertEquals(0, violations.size());
    }

    @Test
    public void validate_xIsSmallIntegerAndYIsBigIntegerAndPropertyTypesAreDifferent_noViolationsOccur(){

        // Given
        TestClass testClass = new TestClass();
        testClass.setX(Long.MIN_VALUE);
        testClass.setY(Double.MAX_VALUE);

        // When
        Set<ConstraintViolation<TestClass>> violations = validator.validate(testClass);

        // Assert
        assertEquals(0, violations.size());
    }

    @Test
    public void validate_xIsBigIntegerAndYIsSmallIntegerAndPropertyTypesAreDifferent_oneViolationOccurs(){

        // Given
        TestClass testClass = new TestClass();
        testClass.setX(Double.MAX_VALUE);
        testClass.setY(Long.MIN_VALUE);

        // When
        Set<ConstraintViolation<TestClass>> violations = validator.validate(testClass);

        // Expect
        String expectedMessage = "The value of property 'x' (179,769,313,486,231,570,000,000,000,000,000,000,000," +
            "000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000," +
            "000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000," +
            "000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000," +
            "000,000,000,000,000,000,000,000,000,000,000,000,000,000,000) is greater than the value of property 'y' " +
            "(-9,223,372,036,854,775,808)."
        ;

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<TestClass> violation = violations.iterator().next();
        assertEquals(NumberPropertyIsNotGreaterThanOther.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals(expectedMessage, violation.getMessage());
    }

    @Test
    public void validate_xAndYAreDecimalsAndXIsSmallerThanYAndPropertyTypesAreDifferent_noViolationsOccur(){

        // Given
        TestClass testClass = new TestClass();
        testClass.setX(0.349f);
        testClass.setY(0.35d);

        // When
        Set<ConstraintViolation<TestClass>> violations = validator.validate(testClass);

        // Assert
        assertEquals(0, violations.size());
    }

    @Test
    public void validate_xAndYAreDecimalsAndAreTheSameValueAndPropertyTypesAreDifferent_noViolationOccurs(){

        // Given
        TestClass testClass = new TestClass();
        testClass.setX(0.34999999999999997779553950749686919152736663818359375d);
        testClass.setY(0.34999999999999997779553950749686919152736663818359375f);

        // When
        Set<ConstraintViolation<TestClass>> violations = validator.validate(testClass);

        // Assert
        assertEquals(0, violations.size());
    }

    @Test
    public void validate_xAndYAreDecimalsAndXIsBiggerThanYAndPropertyTypesAreDifferent_noViolationOccurs(){

        // Given
        TestClass testClass = new TestClass();
        testClass.setX(0.35d);
        testClass.setY(0.349f);

        // When
        Set<ConstraintViolation<TestClass>> violations = validator.validate(testClass);

        // Expect
        String expectedMessage = "The value of property 'x' (0.35) is greater than the value of property 'y' (0.349)."
        ;

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<TestClass> violation = violations.iterator().next();
        assertEquals(NumberPropertyIsNotGreaterThanOther.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals(expectedMessage, violation.getMessage());
    }
}
