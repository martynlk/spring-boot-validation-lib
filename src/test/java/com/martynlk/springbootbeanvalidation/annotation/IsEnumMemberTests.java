package com.martynlk.springbootbeanvalidation.annotation;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.executable.ExecutableValidator;
import java.lang.reflect.Method;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@RunWith(SpringRunner.class)
@SpringBootTest
public class IsEnumMemberTests {

    @Autowired private ValidatorFactory validatorFactory;
    private Validator validator;
    private ExecutableValidator executableValidator;
    private Method testMethod;

    public enum TestEnum {
        LOW,
        MEDIUM,
        HIGH
    }

    public class TestClass {

        @IsEnumMember(TestEnum.class) public String testField;

        @IsEnumMember(TestEnum.class)
        public String testMethod(@IsEnumMember(TestEnum.class) String parameter){
            return parameter;
        }
    }

    @Before
    public void setup() {
        this.validator = this
            .validatorFactory
            .getValidator()
        ;

        this.executableValidator = validator.forExecutables();

        try {
            this.testMethod = TestClass.class.getMethod("testMethod", String.class);
        } catch (NoSuchMethodException e) {
            fail(e.getMessage());
        }

        LocaleContextHolder.resetLocaleContext();
    }

    @Test
    public void fieldValidation_fieldIsNull_noViolationOccurs(){

        // Given
        TestClass testClass = new TestClass();
        testClass.testField = null;

        // When
        Set<ConstraintViolation<TestClass>> violations = validator.validate(testClass);

        // Assert
        assertEquals(0, violations.size());
    }

    @Test
    public void fieldValidation_fieldIsEmpty_noViolationOccurs(){

        // Given
        TestClass testClass = new TestClass();
        testClass.testField = "";

        // When
        Set<ConstraintViolation<TestClass>> violations = validator.validate(testClass);

        // Assert
        assertEquals(0, violations.size());
    }

    @Test
    public void fieldValidation_fieldIsBlank_noViolationOccurs(){

        // Given
        TestClass testClass = new TestClass();
        testClass.testField = "    ";

        // When
        Set<ConstraintViolation<TestClass>> violations = validator.validate(testClass);

        // Assert
        assertEquals(0, violations.size());
    }

    @Test
    public void fieldValidation_fieldIsNotBlankAndIsSpelledTheSameAsTheEnumMemberButIsNotTheCorrectCase_oneViolationOccurs(){

        // Given
        TestClass testClass = new TestClass();
        testClass.testField = "loW";

        // When
        Set<ConstraintViolation<TestClass>> violations = validator.validate(testClass);

        // Expect
        String expectedMessage = "The value supplied: 'loW', is not valid. Valid values are: [LOW, MEDIUM, HIGH].";

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<TestClass> violation = violations.iterator().next();
        assertEquals(IsEnumMember.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals(expectedMessage, violation.getMessage());
    }

    @Test
    public void fieldValidation_fieldIsNotBlankAndIsSpelledTheSameAsTheEnumMemberAndIsTheCorrectCaseButHasWhitespace_oneViolationOccurs(){

        // Given
        TestClass testClass = new TestClass();
        testClass.testField = " l oW  ";

        // When
        Set<ConstraintViolation<TestClass>> violations = validator.validate(testClass);

        // Expect
        String expectedMessage = "The value supplied: ' l oW  ', is not valid. Valid values are: [LOW, MEDIUM, HIGH].";

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<TestClass> violation = violations.iterator().next();
        assertEquals(IsEnumMember.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals(expectedMessage, violation.getMessage());
    }

    @Test
    public void fieldValidation_fieldIsNotBlankAndIsExactlyEqualToAnEnumMember_noViolationOccurs(){

        // Given
        TestClass testClass = new TestClass();
        testClass.testField = "LOW";

        // When
        Set<ConstraintViolation<TestClass>> violations = validator.validate(testClass);

        // Assert
        assertEquals(0, violations.size());
    }

    @Test
    public void parameterValidation_parameterIsNull_noViolationOccurs(){

        // Given
        String parameter = null;

        // When
        Set<ConstraintViolation<TestClass>> violations = executableValidator
            .validateParameters(
                new TestClass(),
                testMethod,
                new Object[]{ parameter }
            )
        ;

        // Assert
        assertEquals(0, violations.size());
    }

    @Test
    public void parameterValidation_parameterIsEmpty_noViolationOccurs(){

        // Given
        String parameter = "";

        // When
        Set<ConstraintViolation<TestClass>> violations = executableValidator
            .validateParameters(
                new TestClass(),
                testMethod,
                new Object[]{ parameter }
            )
        ;

        // Assert
        assertEquals(0, violations.size());
    }

    @Test
    public void parameterValidation_parameterIsBlank_noViolationOccurs(){

        // Given
        String parameter = "    ";

        // When
        Set<ConstraintViolation<TestClass>> violations = executableValidator
            .validateParameters(
                new TestClass(),
                testMethod,
                new Object[]{ parameter }
            )
        ;

        // Assert
        assertEquals(0, violations.size());
    }

    @Test
    public void parameterValidation_parameterIsNotBlankAndIsSpelledTheSameAsTheEnumMemberButIsNotTheCorrectCase_oneViolationOccurs(){

        // Given
        String parameter = "mEdiUm";

        // When
        Set<ConstraintViolation<TestClass>> violations = executableValidator
            .validateParameters(
                new TestClass(),
                testMethod,
                new Object[]{ parameter }
            )
        ;

        // Expect
        String expectedMessage = "The value supplied: 'mEdiUm', is not valid. Valid values are: [LOW, MEDIUM, HIGH].";

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<TestClass> violation = violations.iterator().next();
        assertEquals(IsEnumMember.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals(expectedMessage, violation.getMessage());
    }

    @Test
    public void parameterValidation_parameterIsNotBlankAndIsSpelledTheSameAsTheEnumMemberAndIsTheCorrectCaseButHasWhitespace_oneViolationOccurs(){

        // Given
        String parameter = " mE di Um  ";

        // When
        Set<ConstraintViolation<TestClass>> violations = executableValidator
            .validateParameters(
                new TestClass(),
                testMethod,
                new Object[]{ parameter }
            )
        ;

        // Expect
        String expectedMessage = "The value supplied: ' mE di Um  ', is not valid. Valid values are: [LOW, MEDIUM, HIGH].";

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<TestClass> violation = violations.iterator().next();
        assertEquals(IsEnumMember.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals(expectedMessage, violation.getMessage());
    }

    @Test
    public void parameterValidation_parameterIsExactlyEqualToAnEnumMember_noViolationOccurs(){

        // Given
        String parameter = "MEDIUM";

        // When
        Set<ConstraintViolation<TestClass>> violations = executableValidator
            .validateParameters(
                new TestClass(),
                testMethod,
                new Object[]{ parameter }
            )
        ;

        // Assert
        assertEquals(0, violations.size());
    }

    @Test
    public void returnValueValidation_returnValueIsNull_noViolationOccurs(){

        // Given
        String returnValue = null;

        // When
        Set<ConstraintViolation<TestClass>> violations = executableValidator
            .validateReturnValue(
                new TestClass(),
                testMethod,
                returnValue
            )
            ;

        // Assert
        assertEquals(0, violations.size());
    }

    @Test
    public void returnValueValidation_returnValueIsEmpty_noViolationOccurs(){

        // Given
        String returnValue = "";

        // When
        Set<ConstraintViolation<TestClass>> violations = executableValidator
            .validateReturnValue(
                new TestClass(),
                testMethod,
                returnValue
            )
            ;

        // Assert
        assertEquals(0, violations.size());
    }

    @Test
    public void returnValueValidation_returnValueIsBlank_noViolationOccurs(){

        // Given
        String returnValue = "    ";

        // When
        Set<ConstraintViolation<TestClass>> violations = executableValidator
            .validateReturnValue(
                new TestClass(),
                testMethod,
                returnValue
            )
            ;

        // Assert
        assertEquals(0, violations.size());
    }

    @Test
    public void returnValueValidation_returnValueIsNotBlankAndIsSpelledTheSameAsTheEnumMemberButIsNotTheCorrectCase_oneViolationOccurs(){

        // Given
        String returnValue = "mEdiUm";

        // When
        Set<ConstraintViolation<TestClass>> violations = executableValidator
            .validateReturnValue(
                new TestClass(),
                testMethod,
                returnValue
            )
            ;

        // Expect
        String expectedMessage = "The value supplied: 'mEdiUm', is not valid. Valid values are: [LOW, MEDIUM, HIGH].";

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<TestClass> violation = violations.iterator().next();
        assertEquals(IsEnumMember.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals(expectedMessage, violation.getMessage());
    }

    @Test
    public void returnValueValidation_returnValueIsNotBlankAndIsSpelledTheSameAsTheEnumMemberAndIsTheCorrectCaseButHasWhitespace_oneViolationOccurs(){

        // Given
        String returnValue = " mE di Um  ";

        // When
        Set<ConstraintViolation<TestClass>> violations = executableValidator
            .validateReturnValue(
                new TestClass(),
                testMethod,
                returnValue
            )
            ;

        // Expect
        String expectedMessage = "The value supplied: ' mE di Um  ', is not valid. Valid values are: [LOW, MEDIUM, HIGH].";

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<TestClass> violation = violations.iterator().next();
        assertEquals(IsEnumMember.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals(expectedMessage, violation.getMessage());
    }

    @Test
    public void returnValueValidation_returnValueIsExactlyEqualToAnEnumMember_noViolationOccurs(){

        // Given
        String returnValue = "MEDIUM";

        // When
        Set<ConstraintViolation<TestClass>> violations = executableValidator
            .validateReturnValue(
                new TestClass(),
                testMethod,
                returnValue
            )
            ;

        // Assert
        assertEquals(0, violations.size());
    }
}
