package com.martynlk.springbootbeanvalidation.annotation;

import com.martynlk.springbootbeanvalidation.validator.IsNullValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Mark an object as needing to be null. This annotation can
 * be applied to an Object's field values, a method parameter,
 * and a method return value.
 */
@Documented
@Target({FIELD, PARAMETER, METHOD, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = IsNullValidator.class)
public @interface IsNull {

    String message() default "com.martynlk.springbootbeanvalidation.annotation.IsNull.message";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
