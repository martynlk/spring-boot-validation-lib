package com.martynlk.springbootbeanvalidation.annotation;

import com.martynlk.springbootbeanvalidation.validator.NumberPropertyIsNotGreaterThanOtherValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Mark two fields that are sub-classes of {@link java.lang.Number} in an Object for comparison.
 * <\p>
 * If the property indicated by {@link NumberPropertyIsNotGreaterThanOther#propertyToCheck()} has
 * a value greater than the property indicated by {@link NumberPropertyIsNotGreaterThanOther#propertyToCheckAgainst()},
 * a {@link javax.validation.ConstraintViolation} will be raised.
 * </p>
 * If either property value is {@code null}, no {@link javax.validation.ConstraintViolation}
 * will be raised.
 * </p>
 * The values for {@link NumberPropertyIsNotGreaterThanOther#propertyToCheck()} and
 * {@link NumberPropertyIsNotGreaterThanOther#propertyToCheckAgainst()} can be any format
 * allowed by {@link org.apache.commons.beanutils.PropertyUtilsBean}.
 *
 * @author Martyn Lloyd-Kelly {@literal <martynlloydkelly@gmail.com>}
 */
@Documented
@Target({TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = NumberPropertyIsNotGreaterThanOtherValidator.class)
public @interface NumberPropertyIsNotGreaterThanOther {

    String propertyToCheck();
    String propertyToCheckAgainst();

    String message() default "com.martynlk.springbootbeanvalidation.annotation.NumberPropertyIsNotGreaterThanOther.message";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
