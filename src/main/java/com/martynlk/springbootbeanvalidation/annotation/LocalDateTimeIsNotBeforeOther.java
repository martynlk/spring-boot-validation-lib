package com.martynlk.springbootbeanvalidation.annotation;

import com.martynlk.springbootbeanvalidation.validator.LocalDateTimeIsNotBeforeOtherValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Mark two fields that are {@link java.time.LocalDateTime LocalDateTimes} in an Object for comparison.
 * <p>
 * <p>
 * The values for {@link LocalDateTimeIsNotBeforeOther#propertyNameToCheck()} and
 * {@link LocalDateTimeIsNotBeforeOther#propertyNameToCheckAgainst()} can be any format
 * allowed by {@link org.apache.commons.beanutils.PropertyUtilsBean}.
 *
 * @author Martyn Lloyd-Kelly {@literal <martynlloydkelly@gmail.com>}
 */
@Documented
@Target(TYPE)
@Retention(RUNTIME)
@Constraint(validatedBy = LocalDateTimeIsNotBeforeOtherValidator.class)
public @interface LocalDateTimeIsNotBeforeOther {

    String propertyNameToCheck();
    String propertyNameToCheckAgainst();
    
    String message() default "com.martynlk.springbootbeanvalidation.annotation.LocalDateTimeIsNotBeforeOther.message";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
