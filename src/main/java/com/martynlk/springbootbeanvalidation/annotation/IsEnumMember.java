package com.martynlk.springbootbeanvalidation.annotation;

import com.martynlk.springbootbeanvalidation.validator.IsEnumMemberValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Mark a {@link String} as needing to be a member of the {@link Enum} class
 * specified as the {@code enumClass} value (required).
 * <br/>
 * <br/>
 * This annotation can be applied to an Object's field values, a method parameter,
 * a method return value, a local method variable, or used to compose other
 * annotations.
 * <br/>
 * <br/>
 * Validation will only succeed if the annotated {@link String} is {@code null},
 * {@code ""}, composed solely of whitespace, or is exactly equal to a member of
 * the {@link Enum} class specified.
 */
@Documented
@Target({FIELD, PARAMETER, METHOD, LOCAL_VARIABLE, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = IsEnumMemberValidator.class)
public @interface IsEnumMember {
    Class<? extends Enum<?>> value();
    String message() default "com.martynlk.springbootbeanvalidation.annotation.IsEnumMember.message";
    Class<?>[] groups() default { };
    Class<? extends Payload>[] payload() default { };
}
