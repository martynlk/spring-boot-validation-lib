package com.martynlk.springbootbeanvalidation.validator;

import com.martynlk.springbootbeanvalidation.annotation.LocalDateTimeIsNotBeforeOther;
import org.apache.commons.beanutils.PropertyUtilsBean;

import javax.validation.ConstraintValidatorContext;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * @author Martyn Lloyd-Kelly {@literal <martynlloydkelly@gmail.com>}
 */
public class LocalDateTimeIsNotBeforeOtherValidator extends BaseValidator<LocalDateTimeIsNotBeforeOther, Object> {
    
    private String messageKey;
    private String propertyNameToCheck;
    private String propertyNameToCheckAgainst;
    private PropertyUtilsBean propertyUtilsBean = new PropertyUtilsBean();
    
    @Override
    public void initialize(LocalDateTimeIsNotBeforeOther constraintAnnotation) {
        this.messageKey = constraintAnnotation.message();
        this.propertyNameToCheck = constraintAnnotation.propertyNameToCheck();
        this.propertyNameToCheckAgainst = constraintAnnotation.propertyNameToCheckAgainst();
    }

    @Override
    public boolean isValid(Object object, ConstraintValidatorContext context) {

        try {

            LocalDateTime valueToCheck = (LocalDateTime)propertyUtilsBean.getProperty(object, propertyNameToCheck);
            LocalDateTime valueToCheckAgainst = (LocalDateTime)propertyUtilsBean.getProperty(object, propertyNameToCheckAgainst);

            if(
                Objects.nonNull(valueToCheck) &&
                Objects.nonNull(valueToCheckAgainst) &&
                valueToCheck.isBefore(valueToCheckAgainst)
            ) {

                return super.addConstraintViolation(
                    context,
                    null,
                    messageKey,
                    propertyNameToCheck,
                    valueToCheck,
                    propertyNameToCheckAgainst,
                    valueToCheckAgainst
                );
            }

        } catch(Exception e) {
            throw new RuntimeException(e);
        }

        return true;
    }

}
