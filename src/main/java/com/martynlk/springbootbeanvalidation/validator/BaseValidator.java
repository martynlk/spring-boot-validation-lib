package com.martynlk.springbootbeanvalidation.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.annotation.Annotation;
import java.util.Locale;
import java.util.Objects;

@Component
public abstract class BaseValidator<T extends Annotation, S> implements ConstraintValidator<T, S> {
    
    @Autowired private MessageSource messageSource;
    private final Locale locale = LocaleContextHolder.getLocale();

    protected final boolean addConstraintViolation(
        ConstraintValidatorContext context,
        String propertyName,
        String messageKey,
        Object... messageValuesToInterpolate
    ) {
        String message = messageSource
            .getMessage(
                messageKey, 
                messageValuesToInterpolate, 
                locale
            )
        ;
        
        context.disableDefaultConstraintViolation();
        context
            .buildConstraintViolationWithTemplate(message)
            .addPropertyNode(Objects.toString(propertyName, ""))
            .addConstraintViolation()
        ;
        return false;
    }
}
