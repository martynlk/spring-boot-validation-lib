package com.martynlk.springbootbeanvalidation.validator;

import com.martynlk.springbootbeanvalidation.annotation.NumberPropertyIsNotGreaterThanOther;
import org.apache.commons.beanutils.PropertyUtilsBean;

import javax.validation.ConstraintValidatorContext;
import java.math.BigDecimal;
import java.util.Objects;

/**
 *
 * @author Martyn Lloyd-Kelly {@literal <martynlloydkelly@gmail.com>}
 */
public class NumberPropertyIsNotGreaterThanOtherValidator extends BaseValidator<NumberPropertyIsNotGreaterThanOther, Object> {

    private String messageKey;
    private String propertyToCheck;
    private String propertyToCheckAgainst;
    private PropertyUtilsBean propertyUtilsBean = new PropertyUtilsBean();

    @Override
    public void initialize(NumberPropertyIsNotGreaterThanOther constraintAnnotation) {
        this.messageKey = constraintAnnotation.message();
        this.propertyToCheck = constraintAnnotation.propertyToCheck();
        this.propertyToCheckAgainst = constraintAnnotation.propertyToCheckAgainst();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {

        try {
            Number propertyValueToCheck = (Number)propertyUtilsBean.getProperty(value, propertyToCheck);
            Number propertyValueToCheckAgainst = (Number)propertyUtilsBean.getProperty(value, propertyToCheckAgainst);

            if(
                Objects.nonNull(propertyValueToCheck) &&
                Objects.nonNull(propertyValueToCheckAgainst)
            ){

                // Convert Numbers to BigDecimals since this data type can accommodate all sizes of Number. Use the
                // String constructor to create a BigDecimal since it will not produce any decimal point strangeness,
                // i.e. 0.35 being converted to 0.34999999999999997779553950749686919152736663818359375.
                BigDecimal valueToCheck = new BigDecimal((propertyValueToCheck).toString());
                BigDecimal valueToCheckAgainst = new BigDecimal((propertyValueToCheckAgainst).toString());

                if (valueToCheck.compareTo(valueToCheckAgainst) == 1) {
                    return super.addConstraintViolation(
                        context,
                        null,
                        messageKey,
                        propertyToCheck,
                        valueToCheck,
                        propertyToCheckAgainst,
                        valueToCheckAgainst
                    );
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return true;
    }
}
