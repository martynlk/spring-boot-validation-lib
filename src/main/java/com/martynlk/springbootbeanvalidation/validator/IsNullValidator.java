package com.martynlk.springbootbeanvalidation.validator;

import com.martynlk.springbootbeanvalidation.annotation.IsNull;

import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

public class IsNullValidator extends BaseValidator<IsNull, Object> {

    private String messageKey;

    @Override
    public void initialize(IsNull constraintAnnotation) {
        this.messageKey = constraintAnnotation.message();
    }

    @Override
    public boolean isValid(Object objectToCheck, ConstraintValidatorContext context) {

        if(Objects.isNull(objectToCheck)){
            return true;
        }

        return super.addConstraintViolation(
            context,
            null,
            messageKey,
            objectToCheck
        );
    }
}
