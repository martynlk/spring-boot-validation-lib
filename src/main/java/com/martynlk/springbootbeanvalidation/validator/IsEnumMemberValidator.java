package com.martynlk.springbootbeanvalidation.validator;

import com.martynlk.springbootbeanvalidation.annotation.IsEnumMember;
import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;

public class IsEnumMemberValidator extends BaseValidator<IsEnumMember, String> {

    private List<String> enumMembers;
    private String messageKey;

    @Override
    public void initialize(IsEnumMember constraintAnnotation) {
        enumMembers = new ArrayList<>();

        for(Enum enumConstant : constraintAnnotation.value().getEnumConstants()) {
            enumMembers.add(enumConstant.toString());
        }

        this.messageKey = constraintAnnotation.message();
    }

    @Override
    public boolean isValid(String stringToCheck, ConstraintValidatorContext context) {

        if(StringUtils.isBlank(stringToCheck) || enumMembers.contains(stringToCheck)){
            return true;
        }

        return super.addConstraintViolation(
            context,
            null,
            messageKey,
            stringToCheck,
            enumMembers
        );
    }
}
