# Spring Boot Bean Validation Library

This is a collection of validator annotations that I have found to
be of use when building Spring Boot applications.  All validators
defined implement the [Java Bean Validation 2.0 
spec](https://beanvalidation.org/2.0/spec/). If you are contributing
to this library, please ensure that your validator also does so.
